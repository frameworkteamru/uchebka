<?php

return [

// Префиксы
    'prefix' => [
        'web' => [
            'Admin',
        ]
    ],

    // Основные сущности
    'masters' => [
        'Group',
        'Rating',
        'Subject',
    ],

    // Подчененные сущности
    'subordinates' => [

        'Group' => [
            'GroupStudent',
        ],
    ],

    'options' => [
        '--force' => true,
    ],

];