<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include 'extraWebAdminRoutes.php';


Route::get('/', 'DefaultController@index');

Route::get('/create', 'DefaultController@create');
Route::post('/created', 'DefaultController@store');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


