<?php

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    ], function () {




        include 'web\Admin\Hobby\Hobby.php';

        Route::group([
        'prefix' => 'group',
        'as' => 'group.',
        'namespace' => 'Web\Admin\Group',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'GroupController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'GroupController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'GroupController@save']);

        Route::match(['get', 'post'], '{id}/edit',    ['as' => 'edit',   'uses' => 'GroupController@edit']);

        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'GroupController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'GroupController@delete']);

                                    include 'web\Admin/Group/GroupStudent.php';
                            });

    
        Route::group([
        'prefix' => 'rating',
        'as' => 'rating.',
        'namespace' => 'Web\Admin\Rating',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'RatingController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'RatingController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'RatingController@save']);

        Route::get('{id}/edit',    ['as' => 'edit',   'uses' => 'RatingController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'RatingController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'RatingController@delete']);

                });

    
        Route::group([
        'prefix' => 'subject',
        'as' => 'subject.',
        'namespace' => 'Web\Admin\Subject',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'SubjectController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'SubjectController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'SubjectController@save']);

        Route::get('{id}/edit',    ['as' => 'edit',   'uses' => 'SubjectController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'SubjectController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'SubjectController@delete']);

                });

        });
