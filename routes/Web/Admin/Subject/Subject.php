<?php
Route::group([
    'prefix' => 'subject',
    'as' => 'subject.',
    'namespace' => 'Subject',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'SubjectController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'SubjectController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'SubjectController@save']);

    Route::group(['prefix' => "{subject}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'SubjectController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'SubjectController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'SubjectController@delete']);

    });
    
});