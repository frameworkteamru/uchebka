<?php
Route::group([
    'prefix' => 'hobby',
    'as' => 'hobby.',
    'namespace' => 'Web\Admin\Hobby',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'HobbyController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'HobbyController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'HobbyController@save']);

    Route::group(['prefix' => "{hobby}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'HobbyController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'HobbyController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'HobbyController@delete']);

    });
    
});