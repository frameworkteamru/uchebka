<?php
Route::group([
    'prefix' => 'student',
    'as' => 'student.',
    'namespace' => 'Student',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'StudentController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'StudentController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'StudentController@save']);

    Route::group(['prefix' => "{student}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'StudentController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'StudentController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'StudentController@delete']);

    });
    
});