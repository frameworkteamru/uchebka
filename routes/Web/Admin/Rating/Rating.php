<?php
Route::group([
    'prefix' => 'rating',
    'as' => 'rating.',
    'namespace' => 'Rating',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'RatingController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'RatingController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'RatingController@save']);

    Route::group(['prefix' => "{rating}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'RatingController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'RatingController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'RatingController@delete']);

    });
    
});