@extends('layouts.panel')

@section('headerName') Groups @endsection
@section('link')
    <a class="btn btn-success pull-right" href="{{ route('admin.group.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
@endsection

@section('table')
    @if($groups->count())
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th class="text-center">Number of students</th>
                <th class="text-right">OPTIONS</th>
            </tr>
            </thead>

            <tbody>
            @foreach($groups as $item)
                @include('web.admin.group.item')
            @endforeach
            </tbody>
        </table>
        {!! $groups->render() !!}
    @else
        <h3 class="text-center alert alert-info">Empty!</h3>
    @endif

@endsection
