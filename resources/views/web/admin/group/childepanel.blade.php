<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading" >
                        <label >Hobbies filter:</label>
                    </div>
                    <div class="panel-body" >
                        @include('web.admin.group.filter.hobby')
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" >
                        <label >Sorting:</label>
                    </div>
                    <div class="panel-body" >
                        @include('web.admin.group.filter.sort')
                    </div>
                </div>
            </div>

            <div class="col-md-9">

                <div class="panel panel-default">
                    <div class="panel-heading" >
                        <label >Search Date:</label>
                    </div>
                    <div class="panel-body" >
                        @include('web.admin.group.filter.date')
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body" >
                        @include('web.admin.group.filter.letter')
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body" >
                        @include('web.admin.group.students')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>