@php
    $checked = isset($filter['hobbies']) ? $filter['hobbies'] : [];

    unset($filter['hobbies']);
@endphp
<form method="POST" action="{{ route('admin.group.edit', ['id' => request()->route('id'), 'filter' => $filter, 'sort' => $sort]) }}">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @foreach($hobbies as $hobby)
            <div class="checkbox">
            <label><input type="checkbox" name="filter[hobbies][]" value="{{ $hobby->id }}"
                {{ in_array($hobby->id, $checked) ? "checked" : '' }}
            >{{ $hobby->name }}</label>
            </div>
        @endforeach
    </div>
    <div class="well well-sm " >
       <button type="submit" class="btn btn-primary">Send</button>
    </div>
</form>
