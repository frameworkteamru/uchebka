<div class="panel-body" >
    @foreach($alphabet as $letter)
        @php
            $value = isset($filter['letter']) ? $filter['letter'] : '';

            $a = array_merge(
                $filter, ['letter' => $letter]
            );
        @endphp
        <a href="{{ route('admin.group.edit', ['id' => request()->route('id'), 'filter' => $a, 'sort' => $sort]) }}"
            @if ( $value == $letter)
                class="btn btn-primary btn-xs"
            @else
                class="btn btn-default btn-xs"
            @endif
        >{{$letter}}</a>
    @endforeach
</div>

<div class="panel-footer " >
    @php
        unset($filter['letter'])
    @endphp
    <a href="{{ route('admin.group.edit', ['id' => request()->route('id'), 'filter' => $filter, 'sort' => $sort]) }}" class="btn btn-danger" role="button">Reset</a>
</div>




