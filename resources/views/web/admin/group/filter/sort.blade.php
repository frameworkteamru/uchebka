<form  method="POST" action="{{ route('admin.group.edit', ['id'=> request()->route('id'), 'filter' => $filter, 'sort' => $sort]) }}">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label >Sort by:</label>
        <div class="radio">
            <label><input type="radio" name="sort[sorting]" value="id"
                {{ isset($sort['sorting']) ? $sort['sorting'] == 'id' ? 'checked' : '' : 'checked' }}
            >id</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="sort[sorting]" value="name"
                {{ isset($sort['sorting']) ? $sort['sorting'] == 'name' ? 'checked' : '' : 'checked' }}
            >Name</label>
        </div>

        <label >Order by:</label>
        <div class="radio">
            <label><input type="radio" name="sort[order]" value="asc"
                {{ isset($sort['order']) ? $sort['order'] == 'asc' ? 'checked' : '' : 'checked' }}
            >asc</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="sort[order]" value="desc"
                {{ isset($sort['order']) ? $sort['order'] == 'desc' ? 'checked' : '' : 'checked' }}
            >desc</label>
        </div>
    </div>
    <div class="well well-sm " >
        <button type="submit" class="btn btn-primary">Send</button>
    </div>
</form>
