<form class="form-inline" method="POST" action="{{ route('admin.group.edit', ['id'=> request()->route('id'), 'filter' => $filter, 'sort' => $sort]) }}">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @php
                $start  = isset($filter['d']['start']) ? $filter['d']['start'] : '';
                $end    = isset($filter['d']['end'])   ? $filter['d']['end']   : '';
            @endphp
        <label>From:</label>
        <input type="date" class="form-control" placeholder="Enter Date" name="filter[d][start]" value="{{ $start }}">
        <label>to:</label>
        <input type="date" class="form-control" placeholder="Enter Date" name="filter[d][end]" value="{{ $end }}">
    </div>
    <button type="submit" class="btn btn-success">Search</button>
    <button class="btn btn-danger" role="button" name="filter[d]" value="">Reset</button>
</form>
