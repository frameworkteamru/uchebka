@if($students->count())
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Date</th>
                <th>Hobby</th>
                <th class="text-right">OPTIONS</th>
            </tr>
        </thead>

        <tbody>
            @foreach($students as $item)
                @include('web.admin.group.studentsItem')
            @endforeach
        </tbody>
    </table>

    <div class="text-center">  {!! $students->links() !!}</div>
@else
    <h3 class="text-center alert alert-info">Empty!</h3>
@endif
