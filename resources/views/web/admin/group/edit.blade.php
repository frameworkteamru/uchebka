@extends('layouts.panel')

@section('headerName') Group / Edit <a href="{{ route('admin.group.edit', ['id' => $group->id]) }}">#{{ $group->id }}</a> @endsection

@section('table')
    <form action="{{ route('admin.group.update', $group->id) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" class="form-control" value="{{$group->name}}">
        </div>
        <div class="form-group">
            <label>Description:</label>
            <input type="text" name="description" class="form-control" value="{{$group->description}}">
        </div>
        <div class="well well-sm">
            <button type="submit" class="btn btn-primary">Save</button>
            <a class="btn btn-link pull-right" href="{{ route('admin.group.index') }}">  Back</a>
        </div>
    </form>
@endsection


@section('include')
    @include('web.admin.group.childepanel')
@endsection
