<tr>
    <td>{{ $item->id }}</td>
    <td>{{ $item->name }}</td>
    <td>{{ $item->date }}</td>
    <td>
        @foreach($item->hobbies as $hobby)
            {{ $hobby->name }};
            {!! $loop->iteration % 2 == 0 ? '<br/>' : '' !!}
        @endforeach
    </td>
    <td class="text-right">
        <a class="btn  btn-info" href="{{ route('admin.group.student.edit', $item->id) }}">Edit</a>
        <form action="{{ route('admin.group.student.delete', $item->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="student" value="{{$item->id}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn  btn-danger">Delete</button>
        </form>
    </td>
</tr>