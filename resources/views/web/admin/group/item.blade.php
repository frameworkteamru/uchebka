<tr>
    <td>{{ $item->id }}</td>
    <td>{{ $item->name }}</td>
    <td>{{ $item->description }}</td>
    <td class="text-center">{{ $item->students->count() }}</td>
    <td class="text-right">
        <a class="btn btn-info" href="{{ route('admin.group.edit', $item->id) }}"> Edit</a>
        <form action="{{ route('admin.group.delete', $item->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-danger"> Delete</button>
        </form>
    </td>
</tr>