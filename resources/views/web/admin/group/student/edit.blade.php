@extends('layouts.panel')

@section('headerName') Student / Edit #{{$student->id}} @endsection

@section('table')
    <form action="{{ route('admin.group.student.update',$student->id) }}" method="POST">

        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" class="form-control" value="{{$student->name}}">
        </div>

        <div class="form-group">
            <label>Date:</label>
            <input type="date" name="date" class="form-control" value="{{$student->date}}">
        </div>

        <div class="form-group">
            <label for="sel1">Group:</label>
            <select name="group_id" class="form-control" id="sel1">
                @foreach($groups as $group)
                    <option @if($student->group_id == $group->id)
                            selected
                            @endif
                            value="{{$group->id}}">{{$group->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label >Hobbies:</label>
            @foreach($hobbies as $hobby)
                <div class="checkbox">
                    <label><input type="checkbox" name="hobbies[]" value="{{$hobby->id}}"
                        @foreach($student->hobbies as $studentHobby)
                            @if($studentHobby->id == $hobby->id)
                                checked
                            @endif
                        @endforeach
                    >{{$hobby->name}}</label>
                </div>
            @endforeach
        </div>

        <div class="well well-sm" style="margin-top: 15px;">
            <button type="submit" class="btn btn-primary">Save</button>
            <a class="btn btn-link pull-right" href="{{ route('admin.group.student.index') }}">  Back</a>
        </div>

    </form>
@endsection