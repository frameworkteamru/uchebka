@extends('layouts.panel')

@section('headerName') Students @endsection
@section('link')
    <a class="btn btn-success pull-right" href="{{ route('admin.group.student.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
@endsection

@section('table')
    @if($students->count())
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Date</th>
                <th>Group</th>
                <th>Hobby</th>
                <th class="text-right">OPTIONS</th>
            </tr>
            </thead>

            <tbody>
            @foreach($students as $item)
                @include('web.admin.group.student.item')
            @endforeach
            </tbody>
        </table>
        {!! $students->render() !!}
    @else
        <h3 class="text-center alert alert-info">Empty!</h3>
    @endif
@endsection