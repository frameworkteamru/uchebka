
@extends('layouts.panel')

@section('headerName') Student / Create @endsection

@section('table')
    <form action="{{ route('admin.group.student.save') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" class="form-control" value="">
        </div>
        <div class="form-group">
            <label>Date:</label>
            <input type="date" name="date" class="form-control" value="">
        </div>
        <div class="form-group">
            <label for="sel1">Group:</label>
            <select type="text" name="group_id" class="form-control" id="sel1">
                @foreach($groups as $group)
                    <option value="{{$group->id}}">{{$group->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label >Hobbies:</label>
            @foreach($hobbies as $hobby)
                <div class="checkbox">
                    <label><input type="checkbox" name="hobbies[]" value="{{$hobby->id}}" >{{$hobby->name}}</label>
                </div>
            @endforeach
        </div>

        <div class="well well-sm" style="margin-top: 15px;">
            <button type="submit" class="btn btn-primary">Create</button>
            <a class="btn btn-link pull-right" href="{{ route('admin.group.student.index') }}"> Back</a>
        </div>
    </form>
@endsection