
@extends('layouts.panel')

@section('headerName') Group / Create @endsection

@section('table')
    <form action="{{ route('admin.group.save') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" class="form-control" value="">
        </div> <div class="form-group">
            <label>Description:</label>
            <input type="text" name="description" class="form-control" value="">
        </div>

        <div class="well well-sm" style="margin-top: 15px;">
            <button type="submit" class="btn btn-primary">Create</button>
            <a class="btn btn-link pull-right" href="{{ route('admin.group.index') }}"> Back</a>
        </div>
    </form>
@endsection