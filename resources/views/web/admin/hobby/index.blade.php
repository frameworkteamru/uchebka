@extends('layouts.panel')

@section('headerName') Hobby @endsection
@section('link')
    <a class="btn btn-success pull-right" href="{{ route('admin.hobby.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
@endsection

@section('table')
            @if($hobbys->count())
                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th class="text-right">OPTIONS</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($hobbys as $item)
                        @include('web.admin.hobby.item')
                    @endforeach
                    </tbody>
                </table>
                {!! $hobbys->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif
@endsection
