@extends('layouts.panel')

@section('headerName') Subject / Edit #{{ $subject->id }} @endsection

@section('table')
    <form action="{{ route('admin.subject.update', $subject->id) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" class="form-control" value="{{$subject->name}}">
        </div>
        <div class="well well-sm">
            <button type="submit" class="btn btn-primary">Save</button>
            <a class="btn btn-link pull-right" href="{{ route('admin.subject.index') }}">  Back</a>
        </div>
    </form>
@endsection