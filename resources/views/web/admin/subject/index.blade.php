@extends('layouts.panel')

@section('headerName') Subject @endsection
@section('link')
    <a class="btn btn-success pull-right" href="{{ route('admin.subject.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
@endsection

@section('table')
    @if($subjects->count())
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th class="text-right">OPTIONS</th>
            </tr>
            </thead>

            <tbody>
            @foreach($subjects as $item)
                @include('web.admin.subject.item')
            @endforeach
            </tbody>
        </table>
        {!! $subjects->render() !!}
    @else
        <h3 class="text-center alert alert-info">Empty!</h3>
    @endif
@endsection
