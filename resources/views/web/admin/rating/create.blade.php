@extends('layouts.panel')

@section('headerName') Rating / Create @endsection

@section('table')
    <form action="{{ route('admin.rating.save') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="sel1">Student name:</label>
            <select type="text" name="student_id" class="form-control" id="sel1">
                @foreach($students as $student)
                    <option value="{{$student->id}}">{{$student->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="sel1">Subject name:</label>
            <select type="text" name="subject_id" class="form-control" id="sel1">
                @foreach($subjects as $subject)
                    <option value="{{$subject->id}}">{{$subject->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="sel1">Value:</label>
            <select type="text" name="value" class="form-control" id="sel1">
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>
        <div class="well well-sm">
            <button type="submit" class="btn btn-primary">Create</button>
            <a class="btn btn-link pull-right" href="{{ route('admin.rating.index') }}"> Back</a>
        </div>
    </form>
@endsection