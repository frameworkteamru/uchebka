@extends('layouts.panel')

@section('headerName') Ratings @endsection
@section('link')
    <a class="btn btn-success pull-right" href="{{ route('admin.rating.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
@endsection

@section('table')
    @if($ratings->count())
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Student name</th>
                <th>Subject name</th>
                <th>Value</th>
                <th class="text-right">OPTIONS</th>
            </tr>
            </thead>

            <tbody>
            @foreach($ratings as $item)
                @include('web.admin.rating.item')
            @endforeach
            </tbody>
        </table>
        {!! $ratings->render() !!}

    @else
        <h3 class="text-center alert alert-info">Empty!</h3>
    @endif
@endsection
