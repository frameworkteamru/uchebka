<tr>
    <td>{{ $item->id }}</td>
    @foreach($students as $student)
        @if($item->student_id == $student->id)
            <td>{{ $student->name }}</td>
        @endif
    @endforeach
    @foreach($subjects as $subject)
        @if($item->subject_id == $subject->id)
            <td>{{ $subject->name }}</td>
        @endif
    @endforeach
    <td>{{ $item->value }}</td>
    <td class="text-right">
        <a class="btn  btn-info" href="{{ route('admin.rating.edit', $item->id) }}"> Edit</a>
        <form action="{{ route('admin.rating.delete', $item->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="student" value="{{$item->id}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn  btn-danger">Delete</button>
        </form>
    </td>
</tr>