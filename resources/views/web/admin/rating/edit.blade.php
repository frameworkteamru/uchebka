@extends('layouts.panel')

@section('headerName') Rating / Edit #{{ $rating->id }} @endsection

@section('table')
    <form action="{{ route('admin.rating.update', $rating->id) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="sel1">Student name:</label>
            <select name="student_id" class="form-control" id="sel1" >
                @foreach($students as $student)
                    <option @if($rating->student_id == $student->id)
                            selected
                            @endif
                            value="{{$student->id}}">{{$student->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="sel1">Subject name:</label>
            <select name="subject_id" class="form-control" id="sel1" >
                @foreach($subjects as $subject)
                    <option @if($rating->subject_id == $subject->id)
                            selected
                            @endif
                            value="{{$subject->id}}">{{$subject->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="sel1">Value:</label>
            <select type="text" name="value" class="form-control" id="sel1">
                <option value="2">2</option>
                <option value="2">3</option>
                <option value="2">4</option>
                <option value="2">5</option>
            </select>
        </div>
        <div class="well well-sm">
            <button type="submit" class="btn btn-primary">Save</button>
            <a class="btn btn-link pull-right" href="{{ route('admin.rating.index') }}">  Back</a>
        </div>
    </form>
@endsection