<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
</head>
<style>
    div.float
    {
        float: left;
        margin: 25px;
    }
    input, select, button
    {
        margin: 5px 0;
    }
</style>
<body>


    <div class="float">

        <div>
            New Group
        </div>
                        <!--  Form group-->
        <form action="{{ url('created') }}" method="POST" >
            {{ csrf_field() }}
            <input type="hidden" name="table" value="group">

            <div >
                <label >Group name</label>
                <div>
                    <input type="text" name="name" value="">
                </div>
            </div>

            <div>
                <label >Group descriptions</label>
                <div>
                    <input type="text" name="description" value="">
                </div>
            </div>

            <!-- Button -->
            <div>
                <div>
                    <button type="submit">
                        <i></i>Add Group
                    </button>
                </div>
            </div>
        </form>
    </div>


    <div class="float">

        <div>
            New Student
        </div>
            <!--  Form student-->
        <form action="{{ url('created') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="table" value="student">

            <div>

                <label>Student group</label>
                <div>
                    <select size="" name="group_id">
                        @foreach($groups as $group)
                            <option value="{{$group->id}}">{{$group->name}}</option>
                        @endforeach
                    </select>
                </div>

                <label>Student name</label>
                <div>
                    <input type="text" name="name" value="">
                </div>

            </div>

            <div>
                <label>Student's birthday</label>
                <div >
                    <input type="date" name="date" value="">
                </div>
            </div>

            <!-- Button -->
            <div>
                <div>
                    <button type="submit">
                        <i></i>Add Student
                    </button>
                </div>
            </div>
        </form>
    </div>


    <div class="float">
        <div>
            New Subject
        </div>
        <!--  Form subject-->
        <form action="{{ url('created') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="table" value="subject">

            <div>

                <label>Subject name</label>
                <div>
                    <input type="text" name="name" value="">
                </div>

            </div>

            <!-- Button -->
            <div>
                <div>
                    <button type="submit">
                        <i></i>Add Subject
                    </button>
                </div>
            </div>
        </form>
    </div>


    <div class="float">
        <div>
            New Rating
        </div>
        <!--  Form Rating-->
        <form action="{{ url('created') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="table" value="rating">

            <label>Student</label>
            <div>
            <select size="" name="student_id">
                    @foreach($groups as $group)
                        @foreach($group->student as $student)
                            <option value="{{$student->id}}">{{$student->name}}</option>
                        @endforeach
                    @endforeach
                </select>
            </div>

            <label>Subject</label>
            <div>
                <select size="" name="subject_id">
                    @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->name}}</option>
                    @endforeach
                </select>

            </div>

            <div>

                <label>Value</label>
                <div>
                    <input type="text" name="value" value="">
                </div>

            </div>

            <!-- Button -->
            <div>
                <div>
                    <button type="submit">
                        <i></i>Add Subject
                    </button>
                </div>
            </div>
        </form>
    </div>

</body>
</html>