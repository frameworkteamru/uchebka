@extends('layouts.default')

@section('header')
    <h1>Списки студентов по группам:</h1>
@endsection
@section('table')

    <div class="row">

    @foreach ($groups as $group)
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading text-center" >
                    <h3>{{$group->name}}</h3>
                </div>
                <div class="panel-body">

                    @foreach ($group->student as $student)
                        <div class="col-md-6">


                            <h5 style="color:@if ($student->avg == 5)green
                            @elseif($student->avg >= 4.5)yellow
                            @elseif($student->avg <= 3 and $student->avg > 0)red
                            @endif
                                    ">{{$student->name}}</h5>


                            @foreach($subjects as $subject)
                                <p>{{ $subject->name }}:
                                    @if(!empty($student->subject[$subject->id]))
                                        @foreach ($student->subject[$subject->id] as $value)
                                            {{ $value }}
                                        @endforeach
                                    @endif
                                </p>
                            @endforeach
                            <h4>Средняя оценка: {{ number_format($student->avg, 2) }}</h4>
                        </div>
                        @if ($loop->iteration %2 == 0)
                            <div class="clearfix"></div>
                        @endif
                    @endforeach
                    <br/>
                </div>
            </div>
        </div>

    @if ($loop->iteration %2 == 0)
        <div class="clearfix"></div>
    @endif
    @endforeach
    </div>
@endsection
@section('header2')
    <h1>Средние баллы по группам:</h1>
@endsection
@section('table2')
    <div class="row">
    @foreach ($groups as $group)
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading text-center" >
                        <h3>{{$group->name}}</h3>
                    </div>
                    <div class="panel-body">

                        @foreach($subjects as $subject)

                            @if(!empty($group->subject[$subject->id]))
                                <p>{{ $subject->name }}: {{ number_format(array_sum($group->subject[$subject->id])
                                                    / count($group->subject[$subject->id]), 2) }}</p>
                            @endif

                        @endforeach

                        <h4>Средняя оценка: {{ number_format($group->avg, 2) }}</h4>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="text-center">  {!! $groups->links() !!}</div>
@endsection
{{--
@section('footer2')
    <h2>Лучшие студенты: </h2>
    @foreach ($groups as $group)
        @foreach ($group->student as $student)
            @if ($student->avg >= 4.5)
                <b>{{ $student->name }} из {{ str_replace('группа', 'группы', mb_strtolower($group->name)) }}; </b>
            @endif
        @endforeach
    @endforeach
@endsection
--}}
