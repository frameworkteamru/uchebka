@if (!Auth::guest())
    <ul class="nav navbar-nav">
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="{{ route('admin.group.index') }}">Groups <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('admin.group.index') }}">Main</a></li>
                <li><a href="{{ route('admin.group.create') }}">Create</a></li>
            </ul>
        </li>

        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="{{ route('admin.group.student.index') }}" >Students <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('admin.group.student.index') }}">Main</a></li>
                <li><a href="{{ route('admin.group.student.create') }}">Create</a></li>
            </ul>
        </li>

        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="{{ route('admin.subject.index') }}" >Subjects <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('admin.subject.index') }}">Main</a></li>
                <li><a href="{{ route('admin.subject.create') }}">Create</a></li>
            </ul>
        </li>

        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="{{ route('admin.rating.index') }}" >Ratings <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('admin.rating.index') }}">Main</a></li>
                <li><a href="{{ route('admin.rating.create') }}">Create</a></li>
            </ul>
        </li>

        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="{{ route('admin.hobby.index') }}" >Hobby <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('admin.hobby.index') }}">Main</a></li>
                <li><a href="{{ route('admin.hobby.create') }}">Create</a></li>
            </ul>
        </li>
    </ul>
@endif