@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading" >
                    <h1>
                        @yield('header')
                        @yield('link')
                    </h1>
                </div>
                <div class="panel-body">
                    @yield('table')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading" >
                    <h1>
                        @yield('header2')
                        @yield('link2')
                    </h1>
                </div>
                <div class="panel-body">
                    @yield('table2')
                </div>
                <div class="panel-footer">
                    @yield('footer2')
                </div>
            </div>
        </div>
    </div>
    @yield('include')
@endsection