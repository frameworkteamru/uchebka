<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading" >
                <h1>
                    @yield('headerNames')
                    @yield('links')
                </h1>
            </div>
            <div class="panel-body">
                @yield('tables')
            </div>
        </div>
    </div>
</div>

