{% $doggy %}extends('layouts.app')

{% $doggy %}section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> {% $class %} / Edit #{{ ${% $var %}->id }}</h1>
    </div>
{% $doggy %}endsection

{% $doggy %}section('content')

    <div class="row">
        <div class="col-md-12">
@if(count($parent))
            <form action="{{ route('{% $viewPrefix %}.update', [${% $parent['name'] %}, ${% $var %}->id]) }}" method="POST">
@else
            <form action="{{ route('{% $viewPrefix %}.update', ${% $var %}->id) }}" method="POST">
@endif
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
@if(count($parent))
                    <a class="btn btn-link pull-right" href="{{ route('{% $viewPrefix %}.index', ${% $parent['name'] %}) }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
@else
                    <a class="btn btn-link pull-right" href="{{ route('{% $viewPrefix %}.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
@endif
                </div>
            </form>

        </div>
    </div>
{% $doggy %}endsection