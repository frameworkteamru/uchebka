{% $doggy %}extends('layouts.app')

{% $doggy %}section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> {% $class %}
@if(count($parent))
            <a class="btn btn-success pull-right" href="{{ route('{% $viewPrefix %}.create', ['{% $parent['name'] %}' => ${% $parent['name'] %}]) }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
@else
            <a class="btn btn-success pull-right" href="{{ route('{% $viewPrefix %}.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
@endif
        </h1>
    </div>
{% $doggy %}endsection

{% $doggy %}section('content')
    <div class="row">
        <div class="col-md-12">
            {% $doggy %}include('{% $viewPath %}.filter')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {% $doggy %}if(${% $entity %}s->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        {% $doggy %}foreach(${% $entity %}s as $item)
                            {% $doggy %}include('{% $viewPath %}.item')
                        {% $doggy %}endforeach
                    </tbody>
                </table>
                {!! ${% $entity %}s->render() !!}
            {% $doggy %}else
                <h3 class="text-center alert alert-info">Empty!</h3>
            {% $doggy %}endif
        </div>
    </div>
{% $doggy %}endsection