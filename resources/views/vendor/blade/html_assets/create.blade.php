{% $doggy %}extends('layouts.app')

{% $doggy %}section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> {% $class %} / Create </h1>
    </div>
{% $doggy %}endsection

{% $doggy %}section('content')
    <div class="row">
        <div class="col-md-12">
@if(count($parent))
            <form action="{{ route('{% $viewPrefix %}.save', ${% $parent['name'] %}) }}" method="POST">
@else
            <form action="{{ route('{% $viewPrefix %}.save') }}" method="POST">
@endif
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
@if(count($parent))
                    <a class="btn btn-link pull-right" href="{{ route('{% $viewPrefix %}.index', ${% $parent['name'] %}) }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
@else
                    <a class="btn btn-link pull-right" href="{{ route('{% $viewPrefix %}.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
@endif
                </div>
            </form>
        </div>
    </div>
{% $doggy %}endsection