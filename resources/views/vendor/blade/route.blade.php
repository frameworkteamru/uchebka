Route::group([
    'prefix' => '{{ $routePrefix }}',
    'as' => '{{ $routePrefix }}.',
@if(!count($parent))
    'namespace' => '{{ $namespaces['route'] }}',
@endif
], function () {

    Route::get('',             ['as' => 'index',  'uses' => '{{ $controllerClass }}@index']);

    Route::get('create',       ['as' => 'create', 'uses' => '{{ $controllerClass }}@create']);
    Route::post('save',        ['as' => 'save',   'uses' => '{{ $controllerClass }}@save']);

    Route::group(['prefix' => "{{ '{' . $var . '}' }}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => '{{ $controllerClass }}@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => '{{ $controllerClass }}@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => '{{ $controllerClass }}@delete']);

    @foreach($children as $child)
    include __DIR__ . "/{{ $child }}.php";
    @endforeach
});
    
});