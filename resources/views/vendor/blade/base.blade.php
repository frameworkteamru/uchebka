namespace {{ $namespaces['base'] }};

@if ($authenticable)
use Illuminate\Foundation\Auth\User as Model;
@else
use Illuminate\Database\Eloquent\Model;
@endif
@if(isset($vars['cascadeSoftDeletes']))
use Iatstuti\Database\Support\CascadeSoftDeletes;
@endif
@if(isset($vars['softDeletes']))
use Illuminate\Database\Eloquent\SoftDeletes;
@endif

class {{ $class }} extends Model
{
@if(isset($vars['softDeletes']) || isset($vars['cascadeSoftDeletes']))
    {{ isset($vars['softDeletes']) ? "use SoftDeletes" : '' }}{{ isset($vars['cascadeSoftDeletes']) ? ", CascadeSoftDeletes" : ''}};

@endif
    public $timestamps = {{ $vars['timestamp'] ? 'true' : 'false' }};

    protected $table = '{{ $table }}';
@if (isset($vars['cascadeSoftDeletes']))

    protected $cascadeDeletes = [
    @foreach($vars['cascadeSoftDeletes'] as $entry)
    '{{ $entry }}',
    @endforeach
];
@endif

    protected $fillable = [
    @foreach($fillable as $field)
    '{{ $field }}',
    @endforeach
];
<?php $relationships = isset($relationships) ? $relationships : array(); ?>
@forelse($relationships as $name => $rel)

    public function {{ $rel['name'] }}()
    {
@if ($rel['type'] == 'hasMany' || $rel['type'] == 'hasOne')
        return $this->{{ $rel['type'] }}('{{ $rel['path'] }}', '{{ $rel['foreign'] }}', '{{ $rel['local'] }}');
@elseif ($rel['type'] == 'belongsTo')
        return $this->{{ $rel['type'] }}('{{ $rel['path'] }}', '{{ $rel['local'] }}', '{{ $rel['foreign'] }}');
@elseif ($rel['type'] == 'belongsToMany')
        return $this->{{ $rel['type'] }}('{{ $rel['path'] }}', '{{ $rel['relation_table'] }}', '{{ $rel['foreign'] }}', '{{ $rel['local'] }}');
@endif
    }
@empty
@endforelse
}