<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//Студенты
class Student extends Model
{
    public $table = 'group__student';

    public $total = [];
    public $subject = [];
    public $avg;


    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function rating()
    {
        return $this->hasMany('App\Rating');
    }

}
