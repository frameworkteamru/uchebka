<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//Предметы
class Subject extends Model
{
    public $table = 'subject';
    //У каждого предмета МНОГО оценок
    public function rating()
    {
        return $this->hasMany('App\Rating');
    }


}
