<?php
namespace App\Model\Base\Subject;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public $timestamps = false;

    protected $table = 'subject';

    protected $fillable = [
        'name',
    ];

    public function ratings()
    {
        return $this->hasMany('App\Model\Rating\Rating', 'subject_id', 'id');
    }
}