<?php
namespace App\Model\Base\Group;

use Illuminate\Database\Eloquent\Model;

class GroupStudent extends Model
{
    public $timestamps = false;

    protected $table = 'group__student';

    protected $fillable = [
        'group_id',
        'name',
        'date',
    ];

    public function ratings()
    {
        return $this->hasMany('App\Model\Rating\Rating', 'student_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo('App\Model\Group\Group', 'group_id', 'id');
    }

    public function hobbies()
    {
        return $this->belongsToMany('App\Model\Hobby\Hobby', 'students_hobbies', 'student_id','hobby_id');
    }
}