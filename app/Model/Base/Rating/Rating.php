<?php
namespace App\Model\Base\Rating;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public $timestamps = false;

    protected $table = 'rating';

    protected $fillable = [
        'subject_id',
        'student_id',
        'value',
    ];

    public function subject()
    {
        return $this->belongsTo('App\Model\Subject\Subject', 'subject_id', 'id');
    }

    public function student()
    {
        return $this->belongsTo('App\Model\Group\GroupStudent', 'student_id', 'id');
    }
}