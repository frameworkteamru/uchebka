<?php
namespace App\Model\Base\Hobby;

use Illuminate\Database\Eloquent\Model;

class Hobby extends Model
{
    public $timestamps = false;

    protected $table = 'hobby';

    protected $fillable = [
        'name',
    ];

    public function students()
    {
        return $this->belongsToMany('App\Model\Group\GroupStudent', 'students_hobbies', 'hobby_id', 'student_id');
    }
}