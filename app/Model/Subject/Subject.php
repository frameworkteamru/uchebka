<?php
namespace App\Model\Subject;

use App\Model\Base\Subject\Subject as BaseSubject;
use App\Model\Rating\Rating;

class Subject extends BaseSubject
{
    static $listFields = [];

    static $rules = [];
}