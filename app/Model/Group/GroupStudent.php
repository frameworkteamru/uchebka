<?php
namespace App\Model\Group;

use App\Model\Base\Group\GroupStudent as BaseStudent;
use App\Model\Rating\Rating;

class GroupStudent extends BaseStudent
{
    static $listFields = [];

    static $rules = [];

    public function scopeFilter($query, $params)
    {
        if (isset($params['d']['start']) && !empty($params['d']['start']) &&
            isset($params['d']['end']) && !empty($params['d']['end'])) {
            $query->whereBetween('date', [$params['d']['start'], $params['d']['end']]);
        }

        if (isset($params['letter']) && !empty($params['letter'])) {
            $query->where('name', 'LIKE', $params['letter'] . '%');
        }

        if (isset($params['hobbies']) && !empty($params['hobbies'])) {
            $query->whereHas('hobbies', function ($query) use ($params) {
                $query->whereIn('id', $params['hobbies']);
            });
        }

        return $query;
    }

    public function scopeSort($query, $params)
    {
        if (
            isset($params['sorting']) && isset($params['order'])
        ) {
            $query->orderBy($params['sorting'], $params['order']);
        }

        return $query;
    }

}