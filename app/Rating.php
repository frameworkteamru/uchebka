<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public $table = 'rating';


    public static function calculateAvg($groups)
    {
        foreach ($groups as $group)
        {
            foreach ($group->student as $student)
            {
                foreach ($student->rating as $rating)
                {
                    $group->total[] = $rating->value;
                    $group->subject[$rating->subject_id][] = $rating->value;

                    $student->total[] = $rating->value;
                    $student->subject[$rating->subject_id][] = $rating->value;
                }

                //средний бал студента
                if (count($student->total) <> 0)
                {
                    $student->avg = array_sum($student->total) / count($student->total);
                }

            }

            //средний бал группы
            if (count($group->total) <> 0)
            {
                $group->avg = array_sum($group->total) / count($group->total);
            }
        }
    }

    public function student()
    {
        return $this->belongsTo('App\Student');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }
}
