<?php
namespace App\Http\Controllers\Web\Admin\Subject;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Rating\Rating;
use App\Model\Subject\Subject;
use Illuminate\Http\Request;



class SubjectController extends Controller
{

    public function index()
    {
        $subjects = Subject::orderBy('id', 'desc')->paginate(10);

        return view('web.admin.subject.index', [
            'subjects' => $subjects
        ]);
    }

    public function create()
    {
        return view('web.admin.subject.create');
    }

    public function save(Request $request)
    {
        $subject = new Subject($request->all());

        $subject->save();

        return redirect()->route('admin.subject.index');
    }

    public function edit($id)
    {
        $subject = Subject::find($id);

        return view('web.admin.subject.edit', [
            'subject' => $subject
        ]);
    }

    public function update(Request $request, $id)
    {
        Subject::find($id)->update($request->all());

        return redirect()->route('admin.subject.index');
    }

    public function delete($id)
    {
        Subject::destroy($id);

        return redirect()->back();
    }
}