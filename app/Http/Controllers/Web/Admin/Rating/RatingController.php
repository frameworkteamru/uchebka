<?php
namespace App\Http\Controllers\Web\Admin\Rating;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Rating\Rating;
use App\Model\Group\GroupStudent;
use App\Model\Subject\Subject;
use Illuminate\Http\Request;



class RatingController extends Controller
{
    public function index()
    {
        $ratings = Rating::orderBy('id', 'desc')->paginate(10);
        $students = GroupStudent::all();
        $subjects = Subject::all();

        return view('web.admin.rating.index', [
            'ratings' => $ratings,
            'students' => $students,
            'subjects' => $subjects,
        ]);
    }

    public function create()
    {
        $students = GroupStudent::all();
        $subjects = Subject::all();
        return view('web.admin.rating.create', [
            'students' => $students,
            'subjects' => $subjects,
        ]);
    }

    public function save(Request $request)
    {
        $rating = new Rating($request->all());

        $rating->save();

        return redirect()->route('admin.rating.index');
    }

    public function edit($id)
    {
        $rating = Rating::find($id);
        $students = GroupStudent::all();
        $subjects = Subject::all();

        return view('web.admin.rating.edit', [
            'rating' => $rating,
            'students' => $students,
            'subjects' => $subjects,
        ]);
    }

    public function update(Request $request, $id)
    {
        Rating::find($id)->update($request->all());

        return redirect()->route('admin.rating.index');
    }

    public function delete($id)
    {
        Rating::destroy($id);

        return redirect()->back();
    }
}