<?php
namespace App\Http\Controllers\Web\Admin\Hobby;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Hobby\Hobby;
use Illuminate\Http\Request;



class HobbyController extends Controller
{
    public function index()
    {
        $hobbys = Hobby::orderBy('id', 'desc')->paginate(10);

        return view('web.admin.hobby.index', [
            'hobbys' => $hobbys
        ]);
    }

    public function create()
    {
        return view('web.admin.hobby.create');
    }

    public function save(Request $request)
    {
        $hobby = new Hobby($request->all());

        $hobby->save();

        return redirect()->route('admin.hobby.index');
    }

    public function edit($id)
    {
        $hobby = Hobby::find($id);

        return view('web.admin.hobby.edit', [
            'hobby' => $hobby
        ]);
    }

    public function update(Request $request, $id)
    {
        Hobby::find($id)->update($request->all());

        return redirect()->route('admin.hobby.index');
    }

    public function delete($id)
    {
        Hobby::destroy($id);

        return redirect()->back();
    }
}