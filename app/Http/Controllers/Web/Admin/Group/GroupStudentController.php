<?php
namespace App\Http\Controllers\Web\Admin\Group;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Group\Group;
use App\Model\Hobby\Hobby;
use App\Model\Rating\Rating;
use App\Model\Group\GroupStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;


class GroupStudentController extends Controller
{

    public function index()
    {
        $students = GroupStudent::with('hobbies')->orderBy('id', 'desc')->paginate(10);
        $groups = Group::all();

        return view('web.admin.group.student.index', [
            'students' => $students,
            'groups' => $groups,
        ]);
    }

    public function create()
    {
        $groups = Group::all();
        $hobbies = Hobby::all();

        return view('web.admin.group.student.create', [
            'groups' => $groups,
            'hobbies' => $hobbies,
        ]);
    }

    public function save(Request $request)
    {
        $student = new GroupStudent($request->all());

        $student->save();
        $student->hobbies()->sync($request->hobbies);

        return redirect()->route('admin.group.student.index');
    }

    public function edit($id)
    {
        $student = GroupStudent::find($id);
        $groups = Group::all();
        $hobbies = Hobby::all();

        return view('web.admin.group.student.edit', [
            'student' => $student,
            'groups' => $groups,
            'hobbies' => $hobbies,
        ]);
    }

    public function update(Request $request, $id)
    {
        $student = GroupStudent::find($id);

        $student->update($request->all());
        $student->hobbies()->sync($request->hobbies);

        return redirect()->route('admin.group.student.index');
    }

    public function delete($id)
    {
        GroupStudent::destroy($id);

        return redirect()->back();
    }
}