<?php
namespace App\Http\Controllers\Web\Admin\Group;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Group\Group;
use App\Model\Group\GroupStudent;
use App\Model\Hobby\Hobby;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index()
    {
        $groups = Group::orderBy('id', 'desc')->paginate(10);

        return view('web.admin.group.index', [
            'groups' => $groups
        ]);
    }

    public function create()
    {
        return view('web.admin.group.create');
    }

    public function save(Request $request)
    {
        $group = new Group($request->all());

        $group->save();

        return redirect()->route('admin.group.index');
    }

    public function edit(Request $request)
    {
        $hobbies = Hobby::all();
        $group = Group::find($request->route('id'));

        $filter = [];
        if ($request->query('filter') || $request->input('filter')) {
            $filter = array_merge($request->query('filter', []), $request->input('filter', []));
        }

        $sort = ['sorting' => 'id', 'order' => 'asc'];
        if ($request->query('sort') || $request->input('sort')) {
            $sort = array_merge($request->query('sort', []), $request->input('sort', []));
        }

        $qb = GroupStudent::where('group_id', $request->route('id'));

        $qb->filter($filter);

        $qb->sort($sort);

        //dump($qb->toSql(), $filter, $sort);

        $students = $qb->with('hobbies')->paginate(5)->appends([
            'filter' => $filter,
            'sort' => $sort
        ]);

        $alphabet = array();
        foreach (range(chr(0xC0), chr(0xDF)) as $a) {
            $alphabet[] = iconv('CP1251', 'UTF-8', $a);
        }

        return view('web.admin.group.edit',  [
            'filter'    => $filter,
            'sort'      => $sort,
            'group'     => $group,
            'hobbies'   => $hobbies,
            'students'  => $students,
            'alphabet'  =>  $alphabet,
        ]);
    }

    public function update(Request $request)
    {
        Group::find($request->route('id'))->update($request->all());

        return redirect()->route('admin.group.index');
    }

    public function delete($id)
    {
        Group::destroy($id);

        return redirect()->back();
    }
}