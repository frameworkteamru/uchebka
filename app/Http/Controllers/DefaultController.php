<?php

namespace App\Http\Controllers;

use App\Group;
use App\Model\Group\GroupStudent;
use App\Model\Hobby\Hobby;
use App\Rating;
//use Faker\Generator as Faker;
use App\Student;
use App\Subject;
use Illuminate\Http\Request;
use Faker\Provider\ru_RU\Company as Faker;

class DefaultController extends Controller
{
    public function index()
    {
        $groups = Group::with(['student.rating'])->paginate(2);

        Rating::calculateAvg($groups);

        return view('default.index')->with([
            'groups' => $groups,
            'subjects' => Subject::all(),
        ]);
    }

    public function create ()
    {
        $groups = Group::with('student.rating')->get();
        $subjects = Subject::all();

        return view('default.create', ['groups' => $groups,
                                             'subjects' => $subjects,
                                            ]);
}

    public function store (Request $request)
    {
        if ($request->table == 'student')
        {
            $this->validate($request, [
                'name' => 'required|min:1|max:255',
            ]);

            Student::insert(array(
                'group_id' => $request->group_id,
                'name' => $request->name,
                'date' => $request->date,
            ));
        }

        if ($request->table == 'group')
        {
            $this->validate($request, [
                'name' => 'required|min:1|max:255',
                'description' => 'required|max:255',
            ]);

            Group::insert(array(
                'name' => $request->name,
                'description' => $request->description,
            ));
        }

        if ($request->table == 'subject')
        {
            $this->validate($request, [
                'name' => 'required|max:255|min:1',
            ]);

            Subject::insert(array(
                'name' => $request->name,
            ));
        }

        if ($request->table == 'rating')
        {
            $this->validate($request, [
                'value' => 'required|in:2,3,4,5',
            ]);

            Rating::insert(array(
                'student_id' => $request->student_id,
                'subject_id' => $request->subject_id,
                'value' => $request->value,
            ));
        }

        return redirect('/create');
    }

}
