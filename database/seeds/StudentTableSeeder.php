<?php

use Illuminate\Database\Seeder;
use App\Model\Group\GroupStudent;


class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $hobby = \App\Model\Hobby\Hobby::all();

        $students = factory(GroupStudent::class, 1000)->create();

        foreach ($students as $student){
            $array = $faker->randomElements($hobby->pluck('id')->toArray(), $faker->numberBetween(0, $hobby->count()));

            $student->hobbies()->sync($array);
        }
    }
}
