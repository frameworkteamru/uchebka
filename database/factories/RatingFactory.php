<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Model\Rating\Rating::class, function (Faker\Generator $faker) {

    return [
        'student_id' => $faker->randomElement(App\Model\Group\GroupStudent::pluck('id')->toArray()),
        'subject_id' => $faker->randomElement(App\Model\Subject\Subject::pluck('id')->toArray()),
        'value' => $faker->numberBetween(2, 5),
    ];
});
