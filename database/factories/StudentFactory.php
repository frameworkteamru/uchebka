<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Faker\Factory as Faker;


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Model\Group\GroupStudent::class, function () {

    $faker = Faker::create('ru_RU');

    return [
        'name' => $faker->name,
        'group_id' => $faker->randomElement(App\Model\Group\Group::pluck('id')->toArray()),
        'date' => $faker->date($format = 'Y-m-d', $max = '-18 years'),
    ];
});
